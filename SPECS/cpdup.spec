%define debug_package %{nil}
%define repo github.com/DragonFlyBSD/cpdup
Name:           cpdup
Version:        1.22
Release:        1%{?dist}
Summary:        Filesystem mirroring utility from DragonFly BSD

License:        BSD
URL:            https://%{repo}
Source0:        https://%{repo}/archive/v%{version}.tar.gz

Requires:       libbsd
BuildRequires:  libbsd-devel openssl-devel pkgconfig make

%if 0%{?fedora}
BuildRequires:  gcc
%endif

AutoReq:        no 
AutoReqProv:    no

%description
Filesystem mirroring utility from DragonFly BSD

%prep
%setup -q -c

%build
cd %{name}-%{version} && make

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1

cp %{name}-%{version}/%{name} %{buildroot}%{_bindir}
cp %{name}-%{version}/%{name}.1 %{buildroot}%{_mandir}/man1

%files
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1*
%license %{name}-%{version}/LICENSE
%doc %{name}-%{version}/BACKUPS
%doc %{name}-%{version}/PORTING
%doc %{name}-%{version}/README.md

%changelog
* Sat May 01 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.22-1
- Update to version 1.22

* Thu Apr 30 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.21-1
- Update to version 1.21

* Thu Oct 24 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.20-1
- Initial package for version 1.20
